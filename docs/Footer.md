# Footer

The footer is composed of 3 sections.

- PageDetails section.
  - Report a problem
  - Share this page button
  - Date Modified
- About government Links
- Canada.<span>.ca Links

## Share this Page

A button that brings up a panel with a large amount of social media sites to share to.

On panel open, the page content is replaced with the panel.

Panel close is done via the close button, or hitting 'esc'.

On panel close, page focus is manually brought back to share button.
