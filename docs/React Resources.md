# Useful Links

## JavaScript

[ES6 Destructuring & Variable Renaming](https://www.barrymichaeldoyle.com/destructuring/)

## React

[React JS Official Documentation](https://reactjs.org/docs/getting-started.html)

[React Developer Cheat Sheet](https://devhints.io/react)

[React - Uncontrolled vs. Controlled Forms (Usage)](https://reactjs.org/docs/forms.html#controlled-components)

[React - typechecking for components with the prop-types library](https://www.npmjs.com/package/prop-types)

[Krasimir Tsonev - React Design Patterns (github)](https://github.com/krasimir/react-in-patterns)

[Airbnb React Styling Guide](https://airbnb.io/javascript/react/#props)

[this.setState() in React](https://medium.com/@bretdoucette/understanding-this-setstate-name-value-a5ef7b4ea2b4)

# Other Development Tools

[Redux Devtools for Dummies](https://codeburst.io/redux-devtools-for-dummies-74566c597d7)
