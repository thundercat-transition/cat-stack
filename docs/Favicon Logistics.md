# Favicon logistics

To generate the correct favicon files for a fresh favicon, follow the instructions here: https://realfavicongenerator.net/

Once you do, download the favicon package and extract the contents to `./frontend/public` (this should be "Step 1" of your final results)

- Do not carry out step 3 as shown here: ![favicon-generation](/docs/images/favicon-generation.png)

Make sure to go to `manifest.json` and update the properties according to the current project!
