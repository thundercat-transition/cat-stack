# Page Scroll Reset

On a route change, the scroll position does not change, meaning if you've scrolled down, you remain at that position instead of the top of the page.

This was solved by using adding a `window.scrollTo(0,0)` call on a route change using `history.listen`, following the methods used in this article:

- https://medium.com/@nasir/reset-scroll-position-on-change-of-routes-react-a0bd23093dfe

Implementation of the history package in App.js based on the following articles:

- https://support.mouseflow.com/support/solutions/articles/44001535025-tracking-url-changes-with-react
- https://github.com/ReactTraining/history/blob/master/docs/GettingStarted.md
