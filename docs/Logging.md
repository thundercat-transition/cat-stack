# Logging

## Logging framework

The loglevel library (https://github.com/pimterry/loglevel) seems to be the best tool for frontend logging (updated recently, >6mil downloads).

- replaces console.log() with level-based logging/filtering

Django has a page on logging: https://docs.djangoproject.com/en/3.0/topics/logging/

## Log Structure

- Logs should have a timestamp
- Logs should have a level of severity
- Log format should remain consistent to allow easier analysis of logs
  - Possible format: (timestamp) (level) - (contextual information about entry)

### Client-Based Logging

If we are logging information from a client/session, we would need a method of ensuring that we can gather all logs from that specific session

If this does occur we would also need to ensure that no priveleged / user identifiable information is stored in the logs.

## Implementation Questions:

To implement this, we would need to identify:

- what are we logging?

  - api calls?
  - button presses?
  - session id?

  - are we logging solely the backend, or are we logging the frontend as well?

- where are the logs sent?

  - backend db?
  - stored locally client-side until issue?

- what format should the logs be in?

---

# Meeting Minutes on Logging

What do we _NOT_ log?

- Nothing Protected (no personal information like DOB/full name, etc. - extra details): https://www.tpsgc-pwgsc.gc.ca/esc-src/protection-safeguarding/niveaux-levels-eng.html
- No need to log API calls

What do we log?

- Errors
- Performance (e.g. slow performance times)

What do we store/save to logs?

- Save to backend (don't store in DB or in frontend for long term)
- Logs should all be saved in the same location (e.g. weblogics console folder)

Take care of logging events on frontend in case the services are down

- Throw error if API call fails & crash the page, handle it etc.
- Kubernetes

# Frontend logging

React Error Boundaries were used to implement the frontend logging mechanism: https://reactjs.org/docs/error-boundaries.html .

A higher-order component `ErrorWrapper.jsx` is used to wrap around the entire application in `index.js`. This component uses `getDerivedStateFromError()` and `componentDidCatch()` to render the `UnexpectedErrorPage.jsx` component if an error causes the application to crash, and uses the `loglevel` library to log the appropriate error information.

A helper file known as `logUtil.js` contains the core code that handles the actual logging in our application.

The `loglevel-plugin-remote` plugin (included in `logUtil.js`) is used to send the logs (in JSON format) to the backend API (`/api/add-log/`) via a POST request. If the API call fails for any reason, the plugin will automatically re-send the POST request, and will keep retrying at an exponentially reduced frequency until it succeeds, or until the browser is closed. A list of customizations for the remote sending behaviour can be found in their reference: https://github.com/kutuluk/loglevel-plugin-remote

Note that when the error pops up, if you are in the development environment, you will still see the error take up the entire screen over the page. This is normal & the error window tells you this at the very bottom of it, and that the error window will not show up in a production environment if your application crashes due to an error. In dev, you can go to the top-right portion of the window and press the `X` button to close it.

# Backend Logging

Logging configuration for Django were made in `backend/config/settings/base.py`.

- Reference: https://docs.djangoproject.com/en/3.0/topics/logging/

To ensure that the backend container can run properly, a blank `backend/logs/debug.log` file had to be uploaded to the repo.

# References

- OWASP has an excellent logging cheat sheet: https://cheatsheetseries.owasp.org/cheatsheets/Logging_Cheat_Sheet.html

- Kubernetes Logging: https://logz.io/blog/a-practical-guide-to-kubernetes-logging/

- Logging best practices (2017): https://www.loomsystems.com/blog/single-post/2017/01/26/9-logging-best-practices-based-on-hands-on-experience

- Python Logging: https://www.scalyr.com/blog/started-quickly-python-logging

- Application logging best practices: https://www.scalyr.com/blog/application-logging-practices-adopt/

- Monitoring in React using AppInsights: https://www.aaron-powell.com/posts/2019-10-04-implementing-monitoring-in-react-using-appinsights/

- Client-Side Logging / Error Handling in React (Mar 2019): https://www.loggly.com/blog/best-practices-for-client-side-logging-and-error-handling-in-react/

- Sentry Logging (not free): https://sentry.io/welcome/

- LogLevel library: https://github.com/pimterry/loglevel

- Loglevel Plugin Remote: https://github.com/kutuluk/loglevel-plugin-remote

- Frontend logging: https://www.scalyr.com/blog/getting-started-react-logging/
