#!/bin/bash

# Author(s): William Lam

# ======
# Info
# ======
# This script file maps potentially long commands to a simple parameter when running the script from the shell.

# ===============
# Instructions
# ===============
# - Powershell: run "sh ./scripts.sh <parameter>"
# - Bash: run "./scripts.sh <parameter>"

if [[ $# -ne 1 ]]; then
    echo "Please only enter 1 parameter for running this script."
    echo "For more info on existing commands, run this script with the \"help\" parameter."
    exit 2
fi

# ===============
# Command list
# ===============
case $1 in
    "startfront")
        docker-compose -f docker-compose-dev-frontend.yml up
        ;;

    "bashfront")
        docker exec -it cat-stack_frontend_1 bash
        ;;

    "test")
        docker exec -it cat-stack_frontend_1 yarn run test
        ;;

    "stopall")
        docker stop $(docker ps -a -q)
        ;;

    "help")
        help_text
        ;;

    "sql")
        docker exec -it your-project-name_mssql bash -c "/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P \"Canada01!\""
        ;;

    *)
        echo "Unknown command."
        help_text
        ;;
esac


# ===============
# Functions
# ===============

# Prints the help dialog
function help_text()
{
    echo "
        startfront: runs the frontend container only
        bashfront: logs into the frontend container via docker exec
        test: runs Jest in the pre-existing frontend container
        stopall: stops all running Docker containers on the system
        sql: logs into the MS SQL Docker container
    "
}