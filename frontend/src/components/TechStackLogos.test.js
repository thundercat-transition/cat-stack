import React from "react";
import { shallow } from "enzyme";

// Components
import { TechStackLogos } from "./TechStackLogos";

describe("TechStackLogos component", () => {
  it("should render", () => {
    const wrapper = shallow(<TechStackLogos />);
    expect(wrapper.exists()).toBe(true);
  });
});
