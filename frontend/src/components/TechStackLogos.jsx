/**
 * This component contains each of the logos that represent the tech stack
 */
import React from "react";

// SVG logo files
import CRA from "../images/logos/cra-react-logo.svg";
import Django from "../images/logos/django-logo.svg";
import Docker from "../images/logos/docker-logo.svg";
import Gitlab from "../images/logos/gitlab-logo.svg";
import Javascript from "../images/logos/javascript-logo.svg";
import Nginx from "../images/logos/nginx-logo.svg";
import PostgreSQL from "../images/logos/postgresql-logo.svg";
import Python from "../images/logos/python-logo.svg";

const containerStyle = {};

const rowStyle = {};

const colStyle = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
};

const logoParams = {
  margin: "auto",
  width: "40%"
};

const CRAparams = {
  margin: "auto",
  width: "100%"
};

const jsParams = {
  marin: "auto",
  width: "25%"
};

export const TechStackLogos = () => {
  return (
    <>
      <div className="container" style={containerStyle}>
        {/* 1st Row */}
        <div className="row" style={rowStyle}>
          <div className="col-xs-6" style={colStyle}>
            <img src={CRA} className="App-logo" style={CRAparams} alt="logo" />
          </div>
          <div className="col-xs-6" style={colStyle}>
            <img src={Javascript} style={jsParams} alt="logo" />
          </div>
        </div>

        {/* 2nd Row */}
        <div className="row" style={rowStyle}>
          <div className="col-xs-6 col-sm-4" style={colStyle}>
            <img src={Django} style={logoParams} alt="logo" />
          </div>
          <div className="col-xs-6 col-sm-4" style={colStyle}>
            <img src={Python} style={logoParams} alt="logo" />
          </div>
          <div className="col-xs-6 col-sm-4" style={colStyle}>
            <img src={PostgreSQL} style={logoParams} alt="logo" />
          </div>

          <div className="col-xs-6 col-sm-4" style={colStyle}>
            <img src={Nginx} style={logoParams} alt="logo" />
          </div>
          <div className="col-xs-6 col-sm-4" style={colStyle}>
            <img src={Gitlab} style={logoParams} alt="logo" />
          </div>
          <div className="col-xs-6 col-sm-4" style={colStyle}>
            <img src={Docker} style={logoParams} alt="logo" />
          </div>
        </div>
      </div>
    </>
  );
};

export default TechStackLogos;
