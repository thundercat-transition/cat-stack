import React from "react";
import { shallow } from "enzyme";

// Components
import ErrorWrapper from "./ErrorWrapper";

// Reference for unit testing: https://enzymejs.github.io/enzyme/docs/api/ShallowWrapper/simulateError.html
describe("ErrorWrapper component", () => {
  function Something() {
    // this is just a placeholder
    return null;
  }

  const mockEnzymeTestFunc = jest.fn();
  const wrapper = shallow(
    <ErrorWrapper enzymeTestFunc={mockEnzymeTestFunc}>
      <Something />
    </ErrorWrapper>
  );
  const error = new Error(
    "Red console text below this is normal as it is intentionally triggering an error"
  );
  wrapper.find(Something).simulateError(error);

  it("should render", () => {
    expect(wrapper.state("hasError")).toEqual(true);
    expect(mockEnzymeTestFunc.mock.calls.length).toBe(1);
  });
});
