import React from "react";
import { shallow } from "enzyme";
import ReactMarkdown from "react-markdown";

// Components
import { PopupWindow } from "./PopupWindow";

describe("PopupWindow component", () => {
  it("should render", () => {
    const wrapper = shallow(<PopupWindow />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should render the appropriate text in its header, body and footer", () => {
    const props = {
      closeButtonText: "Close Button Test",
      description: "Description Test",
      title: "Title Test"
    };

    const wrapper = shallow(
      <PopupWindow
        closeButtonText={props.closeButtonText}
        description={props.description}
        title={props.title}
      />
    );

    // Header
    expect(
      wrapper
        .find(".modal-title")
        .first()
        .text()
    ).toContain(props.title);

    // Body
    expect(
      wrapper
        .find(".modal-body")
        .first()
        .find(ReactMarkdown)
        .filterWhere(n => n.props().source === props.description).length
    ).toEqual(1);

    // Footer
    expect(
      wrapper
        .find(".modal-footer")
        .first()
        .text()
    ).toContain(props.closeButtonText);
  });

  it("should call its onClick functions if the close buttons are clicked", () => {
    const handleCloseFn = jest.fn();
    const wrapper = shallow(<PopupWindow handleClose={handleCloseFn} />);
    wrapper
      .find("#popup-close-btn")
      .first()
      .simulate("click");

    wrapper
      .find("#popup-close-btn-x")
      .first()
      .simulate("click");
    expect(handleCloseFn.mock.calls.length).toBe(2);
  });
});
