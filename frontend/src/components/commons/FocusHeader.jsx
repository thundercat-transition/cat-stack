import React, { Component } from "react";
import { PropTypes } from "prop-types";

export class FocusHeader extends Component {
  headerRef = React.createRef();

  componentDidMount() {
    this.focusHeader();
  }

  componentDidUpdate() {
    this.focusHeader();
  }

  /**
   * Sets focus to the header if needed
   */
  focusHeader = () => {
    if (this.headerRef.current != null) {
      this.headerRef.current.focus();
    }
  };

  render() {
    const { children } = this.props;
    return (
      <h1 id="wb-cont" tabIndex="-1" ref={this.headerRef}>
        {children}
      </h1>
    );
  }
}

FocusHeader.propTypes = {
  children: PropTypes.node
};

FocusHeader.defaultProps = {
  children: React.Fragment
};

export default FocusHeader;
