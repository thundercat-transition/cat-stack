import React from "react";
import { mount } from "enzyme";
import { MemoryRouter } from "react-router-dom";

// Components
import { PrivateRoute } from "./PrivateRoute";

// eslint-disable-next-line react/prefer-stateless-function
class TestComponent extends React.Component {
  render() {
    return <div>Test Component</div>;
  }
}

// Based off the tests shown here: https://stackoverflow.com/questions/56730186/testing-privateroute-in-react-router-dom
describe("PrivateRoute component", () => {
  it("should render component if user has been authenticated", () => {
    const testPath = "/aprivatepath";
    const wrapper = mount(
      <MemoryRouter initialEntries={[testPath]}>
        <PrivateRoute auth redirectTo={testPath} component={TestComponent} />
      </MemoryRouter>
    );

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    expect(wrapper.exists(TestComponent)).toBe(true);
  });

  it("should redirect if user is not authenticated", () => {
    const testPath = "/aprivatepath";
    const wrapper = mount(
      <MemoryRouter initialEntries={[testPath]}>
        <PrivateRoute auth={false} redirectTo={testPath} component={TestComponent} />
      </MemoryRouter>
    );
    const history = wrapper.find("Router").prop("history");
    expect(history.location.pathname).toBe(testPath);
  });
});
