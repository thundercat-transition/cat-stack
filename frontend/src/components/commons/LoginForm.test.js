import React from "react";
import { shallow } from "enzyme";

// Components
import { LoginForm } from "./LoginForm";

// Constants
import LOCALIZE from "../../resources/textResources";

describe("LoginForm component", () => {
  it("should render", () => {
    const wrapper = shallow(<LoginForm />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should render login error text if given invalid credentials", () => {
    const wrapper = shallow(<LoginForm />);
    wrapper.setState({ wrongCredentials: true });
    expect(wrapper.find("#wrong-credentials-error").text()).toContain(
      LOCALIZE.loginPage.loginError
    );
  });

  // Reference: https://remarkablemark.org/blog/2018/06/13/spyon-react-class-method/
  it("should call the handleLoginSubmit function when attempting to log in", () => {
    const wrapper = shallow(<LoginForm />);
    const spy = jest.spyOn(wrapper.instance(), "handleLoginSubmit").mockImplementation(() => {});
    wrapper.find("form").simulate("submit");
    expect(spy.mock.calls.length).toBe(1);
  });
});
