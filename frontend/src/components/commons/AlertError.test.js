import React from "react";
import { shallow } from "enzyme";

// Components
import { AlertError } from "./AlertError";

describe("AlertError component", () => {
  it("should render", () => {
    const wrapper = shallow(<AlertError />);
    expect(wrapper.exists()).toBe(true);
  });
});
