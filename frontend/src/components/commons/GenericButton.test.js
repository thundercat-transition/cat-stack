import React from "react";
import { shallow } from "enzyme";
import { Link } from "react-router-dom";

// Components
import { GenericButton } from "./GenericButton";

describe("GenericButton component", () => {
  it("should render", () => {
    const wrapper = shallow(<GenericButton />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should render a Link if a linkPath prop is provided & should run its onClick function when clicked", () => {
    const linkPath = "/test";
    const text = "Test text";
    const ariaLabel = "Test aria label";
    const onClickFn = jest.fn();

    const wrapper = shallow(
      <GenericButton
        linkPath={linkPath}
        text={text}
        ariaLabel={ariaLabel}
        onClickFunc={onClickFn}
      />
    );

    const linkComponent = wrapper.find(Link);
    expect(linkComponent.length).toEqual(1);
    expect(linkComponent.text()).toContain(text);

    linkComponent.simulate("click");
    expect(onClickFn.mock.calls.length).toBe(1);
  });

  it("should render a button element if a linkPath prop is not provided  & should run its onClick function when clicked", () => {
    const text = "Test text";
    const ariaLabel = "Test aria label";
    const onClickFn = jest.fn();

    const wrapper = shallow(
      <GenericButton text={text} ariaLabel={ariaLabel} onClickFunc={onClickFn} />
    );

    const buttonElement = wrapper.find("button");
    expect(buttonElement.length).toEqual(1);
    expect(buttonElement.text()).toContain(text);

    buttonElement.simulate("click");
    expect(onClickFn.mock.calls.length).toBe(1);
  });
});
