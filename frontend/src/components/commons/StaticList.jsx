import React from "react";
import { PropTypes } from "prop-types";

// Constants
import LOCALIZE from "../../resources/textResources";

// take an item from that list and convert it to either a list item, or a list item with a link
// since the content served is static, we can use the index as the key
function populateList(item, index) {
  if (item.text !== undefined) {
    if (item.link !== undefined && item.linkText !== undefined) {
      return (
        <li key={index}>
          {LOCALIZE.formatString(item.text, <a href={item.link}>{item.linkText}</a>)}
        </li>
      );
    }
    if (item.bold !== undefined) {
      return (
        <li key={index}>
          {LOCALIZE.formatString(item.text, <span className="boldFormat">{item.bold}</span>)}
        </li>
      );
    }
    return <li key={index}>{item.text}</li>;
  }
  return <li key={index}>{item}</li>;
}

// generates a list of items from an array
const StaticList = props => {
  const { list } = props;
  const listItems = list.map(populateList);
  return <ul>{listItems}</ul>;
};

StaticList.propTypes = {
  list: PropTypes.oneOfType([PropTypes.array, PropTypes.string])
};

StaticList.defaultProps = {
  list: []
};

export default StaticList;
