import React from "react";
import { shallow } from "enzyme";

// Components
import Breadcrumb from "./Breadcrumb";

describe("Breadcrumb component", () => {
  it("should render", () => {
    const wrapper = shallow(<Breadcrumb />);
    expect(wrapper.exists()).toBe(true);
  });
});
