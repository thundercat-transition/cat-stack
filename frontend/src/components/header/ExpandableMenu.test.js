import React from "react";
import { shallow } from "enzyme";

// Components
import ExpandableMenu from "./ExpandableMenu";

describe("ExpandableMenu component", () => {
  it("should render", () => {
    const wrapper = shallow(<ExpandableMenu>Test</ExpandableMenu>);
    expect(wrapper.exists()).toBe(true);
  });
});
