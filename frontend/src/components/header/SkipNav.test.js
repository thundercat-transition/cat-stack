import React from "react";
import { shallow } from "enzyme";

// Components
import SkipNav from "./SkipNav";

describe("SkipNav component", () => {
  it("should render", () => {
    const wrapper = shallow(<SkipNav />);
    expect(wrapper.exists()).toBe(true);
  });
});
