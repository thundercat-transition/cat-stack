import React from "react";

// Constants
import LOCALIZE from "../../resources/textResources";

// Components
import ExpandableMenu from "./ExpandableMenu";
import HeaderLogin from "./HeaderLogin";

export const HeaderMenu = () => {
  return (
    <nav
      className="gcweb-v2 gcweb-menu wb-init gcweb-menu-inited"
      typeof="SiteNavigationElement"
      id="wb-auto-2"
      aria-labelledby="headerMenu"
    >
      <div className="container">
        <h2 className="wb-inv" id="headerMenu">
          Menu
        </h2>
        <ExpandableMenu>
          <ul role="menu" aria-orientation="vertical">
            {LOCALIZE.header.menu.map((e, index) => {
              // map header array to menu items
              return (
                // eslint-disable-next-line react/no-array-index-key
                <li role="presentation" key={`expandableMenuItem${index}`}>
                  <a role="menuitem" href={e[1]}>
                    {e[0]}
                  </a>
                </li>
              );
            })}
          </ul>
        </ExpandableMenu>

        <HeaderLogin />
      </div>
    </nav>
  );
};

export default HeaderMenu;
