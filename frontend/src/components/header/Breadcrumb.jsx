import React from "react";

// Components
import LOCALIZE from "../../resources/textResources";

const navigationStyle = {
  paddingLeft: "1em",
  paddingRight: "1em"
};

const Breadcrumb = () => {
  return (
    <nav role="navigation" id="wb-bc" style={navigationStyle} property="breadcrumb">
      <h2>{LOCALIZE.header.breadcrumb.header}</h2>
      <div className="container">
        <div className="row">
          <ol className="breadcrumb">
            <li>
              <a href={LOCALIZE.header.breadcrumb.homeLink}>
                {LOCALIZE.header.breadcrumb.homeText}
              </a>
            </li>
            <li>
              <a href={LOCALIZE.header.breadcrumb.pscLink}>{LOCALIZE.header.breadcrumb.pscText}</a>
            </li>
            <li>
              <a href={LOCALIZE.header.breadcrumb.breadcrumbLink}>
                {LOCALIZE.header.breadcrumb.breadcrumbText}
              </a>
            </li>
          </ol>
        </div>
      </div>
    </nav>
  );
};

export default Breadcrumb;
