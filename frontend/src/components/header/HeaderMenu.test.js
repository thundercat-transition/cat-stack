import React from "react";
import { shallow } from "enzyme";

// Components
import HeaderMenu from "./HeaderMenu";

describe("HeaderMenu component", () => {
  it("should render", () => {
    const wrapper = shallow(<HeaderMenu />);
    expect(wrapper.exists()).toBe(true);
  });
});
