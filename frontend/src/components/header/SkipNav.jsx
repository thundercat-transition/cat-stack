import React from "react";

// Constants
import LOCALIZE from "../../resources/textResources";

/*
  Skip Navigation component
  Links to main content and footer section for accessibility purposes
*/

export default function SkipNav() {
  return (
    <nav aria-label="Navigation">
      <ul id="wb-tphp" className="wb-init wb-disable-inited">
        <li className="wb-slc">
          <a className="wb-sl" href="#wb-cont">
            {LOCALIZE.navigation.a11y.skipToMain}
          </a>
        </li>
        <li className="wb-slc">
          <a className="wb-sl" href="#wb-info">
            {LOCALIZE.navigation.a11y.skipToFooter}
          </a>
        </li>
      </ul>
    </nav>
  );
}
