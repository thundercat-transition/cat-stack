import React from "react";
import { shallow } from "enzyme";

// Components
import Header from "./Header";
import SearchBox from "./SearchBox";
import ChangeLanguage from "./ChangeLanguage";
import Breadcrumb from "./Breadcrumb";

describe("Header component", () => {
  it("should render", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should have a language switch button", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find(ChangeLanguage)).toHaveLength(1);
  });

  it("should have a search bar", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find(SearchBox)).toHaveLength(1);
  });

  it("should have a breadcrumbs section", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.find(Breadcrumb)).toHaveLength(1);
  });
});
