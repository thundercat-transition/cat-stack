import React from "react";
import { shallow } from "enzyme";

// Components
import { ChangeLanguage } from "./ChangeLanguage";

describe("ChangeLanguage component", () => {
  it("should render", () => {
    const wrapper = shallow(<ChangeLanguage />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should have a button to switch the language that calls setLanguage", () => {
    const mockSetLanguage = jest.fn();

    const wrapper = shallow(<ChangeLanguage setLanguage={mockSetLanguage} />);
    wrapper.findWhere(n => n.props().id === "switchLangButton").simulate("click");
    expect(mockSetLanguage.mock.calls).toHaveLength(1);
  });
});
