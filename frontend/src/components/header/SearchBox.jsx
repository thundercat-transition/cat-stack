import React from "react";

// Constants
import LOCALIZATION from "../../resources/textResources";

const SearchBox = () => {
  return (
    <section id="wb-srch" className="col-xs-12 col-md-4 offset-md-2 text-right">
      <h2>{LOCALIZATION.header.search}</h2>
      <form
        className="form-inline pull-right"
        action={LOCALIZATION.header.searchLink}
        method="get"
        name="cse-search-box"
        role="search"
      >
        <div className="form-group">
          <label htmlFor="wb-srch-q" className="wb-inv">
            {LOCALIZATION.header.searchDefault}
          </label>
          <input
            id="wb-srch-q"
            list="wb-srch-q-ac"
            className="wb-srch-q form-control"
            style={{ display: "inline", width: "auto" }}
            name="q"
            size="34"
            maxLength="170"
            placeholder={LOCALIZATION.header.searchDefault}
            type="search"
          />
          <button
            type="submit"
            id="wb-srch-sub"
            className="btn btn-primary btn-small"
            name="wb-srch-sub"
          >
            <span className="glyphicon-search glyphicon" />
            <span className="wb-inv">{LOCALIZATION.header.search}</span>
          </button>
        </div>
      </form>
    </section>
  );
};

export default SearchBox;
