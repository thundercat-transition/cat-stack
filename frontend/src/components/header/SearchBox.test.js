import React from "react";
import { shallow } from "enzyme";

// Components
import SearchBox from "./SearchBox";

describe("SearchBox component", () => {
  it("should render", () => {
    const wrapper = shallow(<SearchBox />);
    expect(wrapper.exists()).toBe(true);
  });
});
