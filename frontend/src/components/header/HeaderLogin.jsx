import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Redux
import { setLoginState } from "../../redux/loginActions";

// Constants
import LOCALIZE from "../../resources/textResources";
import { PATH } from "../../resources/constants";

// Components
const loginTextStyle = {
  textAlign: "right",
  float: "right",
  marginTop: "0.5em",
  marginRight: "1em"
};

export const HeaderLogin = props => {
  const { isLoggedIn, setLoginState, username } = props;

  // Don't render the login URL text on the actual login page
  if (window.location.pathname === PATH.login) {
    return <></>;
  }

  if (isLoggedIn) {
    return (
      <>
        <div style={loginTextStyle}>
          {username && `${LOCALIZE.header.loginState.usernamePrefix} ${username}`}
          <br />
          <Link
            to={PATH.home}
            id="header-login-text"
            onClick={() => {
              setLoginState(!isLoggedIn);
            }}
          >
            {LOCALIZE.header.loginState.logoutText}
          </Link>
        </div>
      </>
    );
  }

  // Logged out
  return (
    <>
      <Link style={loginTextStyle} to={PATH.login}>
        {LOCALIZE.header.loginState.loginText}
      </Link>
    </>
  );
};

HeaderLogin.propTypes = {
  isLoggedIn: PropTypes.bool,
  setLoginState: PropTypes.func,
  username: PropTypes.string
};

HeaderLogin.defaultProps = {
  isLoggedIn: false,
  setLoginState: () => {},
  username: null
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localizationReducer.language,
    isLoggedIn: state.loginReducer.isLoggedIn,
    username: state.loginReducer.userInfo.username
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ setLoginState }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HeaderLogin);
