import React from "react";
import PropTypes from "prop-types";

// Constants
import LOCALIZE from "../../resources/textResources";

/*
  Implements the Canada.ca date modified pattern  : https://design.canada.ca/common-design-patterns/date-modified.html

  Usage:
    <DateModified date="2020-12-31" /> 
*/

export default function DateModified({ date }) {
  return (
    <dl id="wb-dtmd">
      <dt>{LOCALIZE.pageDetails.dateModifiedText}</dt>
      <dd>
        <time property="dateModified">{date}</time>
      </dd>
    </dl>
  );
}

DateModified.propTypes = {
  date: PropTypes.string.isRequired
};
