import React from "react";

// Constants
import LOCALIZE from "../../resources/textResources";
import GovernmentSymbol from "../../images/wmms-spl.svg";

export default function FooterLinks() {
  return (
    <footer id="wb-info">
      <div className="landscape">
        <nav className="container wb-navcurr" role="navigation" aria-labelledby="footerAboutGov">
          <h2 className="wb-inv" id="footerAboutGov">
            {LOCALIZE.footer.h2}
          </h2>
          <ul className="list-unstyled colcount-sm-2 colcount-md-3">
            <li>
              <a href={LOCALIZE.footer.contactLink}>{LOCALIZE.footer.contact}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.depLink}>{LOCALIZE.footer.dep}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.publicServiceLink}>{LOCALIZE.footer.publicService}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.newsLink}>{LOCALIZE.footer.news}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.lawsLink}>{LOCALIZE.footer.laws}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.reportLink}>{LOCALIZE.footer.report}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.primeMinisterLink}>{LOCALIZE.footer.primeMinister}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.systemLink}>{LOCALIZE.footer.system}</a>
            </li>
            <li>
              <a href={LOCALIZE.footer.openGovLink}>{LOCALIZE.footer.openGov}</a>
            </li>
          </ul>
        </nav>
      </div>

      <div className="brand">
        <div className="container">
          <div className="row">
            <nav className="col-md-10 ftr-urlt-lnk" aria-labelledby="footerAboutSite">
              <h2 className="wb-inv" id="footerAboutSite">
                {LOCALIZE.footer.aboutSite}
              </h2>
              <ul>
                <li>
                  <a href={LOCALIZE.footer.socialMediaLink}>
                    <span>{LOCALIZE.footer.socialMedia}</span>
                  </a>
                </li>

                <li>
                  <a href={LOCALIZE.footer.mobileLink}>
                    <span>{LOCALIZE.footer.mobile}</span>
                  </a>
                </li>

                <li>
                  <a href={LOCALIZE.footer.aboutLink}>
                    <span>{LOCALIZE.footer.about}</span>
                  </a>
                </li>

                <li>
                  <a href={LOCALIZE.footer.termsLink}>
                    <span>{LOCALIZE.footer.terms}</span>
                  </a>
                </li>

                <li>
                  <a href={LOCALIZE.footer.privacyLink}>
                    <span>{LOCALIZE.footer.privacy}</span>
                  </a>
                </li>
              </ul>
            </nav>
            <div className="col-xs-6 visible-sm visible-xs tofpg">
              <a href="#wb-cont">
                {LOCALIZE.footer.pageUp} <span className="glyphicon glyphicon-chevron-up" />
              </a>
            </div>
            <div className="col-xs-6 col-md-2 text-right">
              <img src={GovernmentSymbol} alt={LOCALIZE.footer.imageAltText} />
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
