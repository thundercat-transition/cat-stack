import React from "react";
import { shallow } from "enzyme";

// Components
import PageDetails from "./PageDetails";
import DateModified from "./DateModified";

describe("PageDetails component", () => {
  it("should render", () => {
    const wrapper = shallow(<PageDetails dateChanged="2020-03-30" />);
    expect(wrapper.exists()).toBe(true);
  });

  it("should have a date modified component", () => {
    const wrapper = shallow(<PageDetails dateChanged="2020-03-30" />);
    expect(wrapper.find(DateModified)).toHaveLength(1);
  });
});
