import React from "react";

// Constants
import LOCALIZE from "../../resources/textResources";

export default function ReportIssue() {
  return (
    <a
      href={LOCALIZE.footer.reportIssue.buttonLink}
      className="btn btn-default btn-block col-sm-12 col-md-5 col-lg-4 mrgn-tp-sm text-center"
    >
      {LOCALIZE.footer.reportIssue.buttonText}
    </a>
  );
}
