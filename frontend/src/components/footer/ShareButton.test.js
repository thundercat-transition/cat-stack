import React from "react";
import { shallow } from "enzyme";

// Components
import ShareButton from "./ShareButton";

describe("ShareButton component", () => {
  it("should render", () => {
    const wrapper = shallow(<ShareButton />);
    expect(wrapper.exists()).toBe(true);
  });
});
