import React from "react";
import PropTypes from "prop-types";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Modal from "react-modal";

// CSS imports
import "bootstrap/dist/css/bootstrap.min.css";
import "./GCWeb-6.0/GCWeb/css/theme.css";
import "./css/app-theme.css";
import "./css/create-react-app.css";

// Function/Redux imports
import { getFormattedJWT } from "./helpers/jwtUtil";
import { resetRedux } from "./redux/otherActions";

// Component imports
import RoutesList from "./RoutesList";
import PopupWindow from "./components/commons/PopupWindow";

// Constants imports
import { PATH } from "./resources/constants";
import LOCALIZE from "./resources/textResources";

const history = createBrowserHistory();

export class App extends React.Component {
  state = {
    isShowingTimeoutPopup: false
  };

  componentDidMount = () => {
    Modal.setAppElement("body"); // Fixes a JavaScript error for Modal based on DOM rendering times
  };

  /**
   * Spawn the timeout popup window if the user's auth token expires
   */
  handleCurrentAuthState = async () => {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      const authToken = await getFormattedJWT();

      // Token is invalid, so log the user out by resetting Redux & display the timeout popup window
      if (authToken === null) {
        this.setState({ isShowingTimeoutPopup: true });
        const { resetRedux } = this.props;
        resetRedux();
      }
    }
  };

  /**
   * Functions to run when the timeout popup window is closed
   * Resets the Redux state when executed & redirects to Splash Page afterwards
   */
  closeTimeoutPopup = () => {
    this.setState({ isShowingTimeoutPopup: false });
    history.push(PATH.home);
  };

  render() {
    const { isShowingTimeoutPopup } = this.state;
    return (
      <>
        <div onBlur={this.handleCurrentAuthState}>
          <PopupWindow
            isDisplayed={isShowingTimeoutPopup}
            handleClose={() => this.closeTimeoutPopup}
            title={LOCALIZE.timeoutPopup.title}
            description={LOCALIZE.timeoutPopup.description}
            closeButtonText={LOCALIZE.timeoutPopup.closeButton}
          />
          <Router history={history}>
            <RoutesList />
          </Router>
        </div>
      </>
    );
  }
}

App.propTypes = {
  isLoggedIn: PropTypes.bool,
  resetRedux: PropTypes.func
};

App.defaultProps = {
  isLoggedIn: false,
  resetRedux: () => {}
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localizationReducer.language,
    isLoggedIn: state.loginReducer.isLoggedIn
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ resetRedux }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
