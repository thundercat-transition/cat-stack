import React from "react";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Constants
import { PATH } from "../resources/constants";
import LOCALIZE from "../resources/textResources";

// Helpers
import { sendDataToSampleEndpoint } from "../helpers/apiUtil";

// Components
import GenericButton from "../components/commons/GenericButton";
import PageTemplate from "../components/PageTemplate";
import FocusHeader from "../components/commons/FocusHeader";
import TechStackLogos from "../components/TechStackLogos";
import AlertError from "../components/commons/AlertError";

export class HomePage extends React.Component {
  state = {
    showError: false,
    focusAlert: false
  };

  alertRef = React.createRef();

  componentDidUpdate() {
    this.focusAlert();
  }

  /**
   * Sets focus to the alert
   */
  focusAlert = () => {
    const { focusAlert } = this.state;

    if (focusAlert) {
      if (this.alertRef.current != null) {
        this.alertRef.current.focus();
      }
    }
  };

  render() {
    const { currentLanguage } = this.props;
    const dataForPOST = {
      name: "Test name",
      test_id: "3",
      content: "Testing content to check if it works!"
    };

    const { showError } = this.state;

    return (
      <PageTemplate title={LOCALIZE.homePage.title}>
        <FocusHeader>{LOCALIZE.homePage.title}</FocusHeader>

        {/* create-react-app spinning logo */}
        <header className="row App-header">
          {showError && <AlertError text={LOCALIZE.alerts.message} ref={this.alertRef} />}
          <TechStackLogos />
          <p>{LOCALIZE.homePage.content1}</p>
          <p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              {LOCALIZE.homePage.content2}
            </a>
          </p>
          <GenericButton
            onClickFunc={sendDataToSampleEndpoint}
            data={dataForPOST}
            text={LOCALIZE.homePage.postAPIButton}
            ariaLabel={LOCALIZE.homePage.postAPIButtonLabel}
          />

          <GenericButton
            onClickFunc={() =>
              this.setState(oldState => {
                return { ...oldState, showError: true, focusAlert: true };
              })
            }
            text={LOCALIZE.homePage.errorButton}
          />

          <div className="button-bar">
            <GenericButton
              linkPath={PATH.example}
              text={LOCALIZE.navigation.nextButton} // to prevent screenreaders from reading misc. text like ">>", add an aria-label without the characters
              ariaLabel={LOCALIZE.navigation.nextButtonLabel}
            />
          </div>

          <p>currentLanguage: {currentLanguage}</p>
        </header>
      </PageTemplate>
    );
  }
}

HomePage.propTypes = {
  currentLanguage: PropTypes.string
};

HomePage.defaultProps = {
  currentLanguage: "en"
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localizationReducer.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
