import React from "react";
import { shallow } from "enzyme";

// Components
import { UnexpectedErrorPage } from "./UnexpectedErrorPage";

describe("Unexpected Error page tests", () => {
  it("should render the Unexpected Error page if the results page does not have the correct props", () => {
    const wrapper = shallow(<UnexpectedErrorPage />);

    expect(wrapper.exists()).toBe(true);
  });
});
