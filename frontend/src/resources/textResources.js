import LocalizedStrings from "react-localization";

const LOCALIZE = new LocalizedStrings({
  // ======================
  // ENGLISH
  // ======================
  en: {
    navigation: {
      nextButton: "Next ››",
      nextButtonLabel: "Next",
      previousButton: "‹‹ Previous",
      previousButtonLabel: "Previous",
      startButton: "Start",
      finishButton: "Finish",
      newActivityButton: "New Activity",
      questionnaireButton: "Questionnaire",

      a11y: {
        skipToMain: "Skip to main content",
        skipToFooter: `Skip to "About government"`
      }
    },

    language: {
      otherLanguage: "Français",
      otherLanguageCode: "fr"
    },

    alerts: {
      errorAlt: "Error",
      message: "Error message here"
    },

    timeoutPopup: {
      title: "Session Expired",
      description:
        "Your session has expired due to inactivity. You will now be redirected to the home page.",
      closeButton: "Close"
    },

    pageDetails: {
      dateModifiedText: "Date modified: "
    },

    header: {
      otherLanguage: "Français",
      otherLanguageCode: "fr",
      canadaHomeLink: "https://www.canada.ca/en.html",
      canadaHomeText: "Government of Canada",

      langSelectSection: "Language selection",

      search: "Search",
      searchLink: "https://www.canada.ca/en/sr/srb.html",
      searchDefault: "Search Canada.ca",

      menu: [
        ["Jobs and the workplace", "https://www.canada.ca/en/services/jobs.html"],
        [
          "Immigration and citizenship",
          "https://www.canada.ca/en/services/immigration-citizenship.html"
        ],
        ["Travel and tourism", "https://travel.gc.ca/"],
        ["Business and industry", "https://www.canada.ca/en/services/business.html"],
        ["Benefits", "https://www.canada.ca/en/services/benefits.html"],
        ["Health", "https://www.canada.ca/en/services/health.html"],
        ["Taxes", "https://www.canada.ca/en/services/taxes.html"],
        ["Environment and natural resources", "https://www.canada.ca/en/services/environment.html"],
        ["National security and defence", "https://www.canada.ca/en/services/defence.html"],
        ["Culture, history and sport", "https://www.canada.ca/en/services/culture.html"],
        ["Policing, justice and emergencies", "https://www.canada.ca/en/services/policing.html"],
        ["Transport and infrastructure", "https://www.canada.ca/en/services/transport.html"],
        ["Canada and the world", "https://international.gc.ca/world-monde/index.aspx?lang=eng"],
        ["Money and finances", "https://www.canada.ca/en/services/finance.html"],
        ["Science and innovation", "https://www.canada.ca/en/services/science.html"]
      ],

      loginState: {
        loginText: "Log in",
        logoutText: "Log out",
        usernamePrefix: "Logged in as:"
      },

      breadcrumb: {
        header: "Breadcrumb trail",
        homeText: "Home",
        homeLink: "http://www.canada.ca/en/index.html",

        pscText: "PSC Home",
        pscLink: "https://www.canada.ca/en/public-service-commission.html",

        breadcrumbText: "Breadcrumb trail",
        breadcrumbLink: "https://design.canada.ca/common-design-patterns/breadcrumb-trail.html"
      }
    },

    footer: {
      h2: "About government",
      feedback: "Feedback",
      feedbackLink: "https://www1.canada.ca/en/contact/feedback.html",
      socialMedia: "Social media",
      socialMediaLink: "https://www.canada.ca/en/social.html",
      mobile: "Mobile applications",
      mobileLink: "https://www.canada.ca/en/mobile.html",
      about: "About Canada.ca",
      aboutLink: "https://www1.canada.ca/en/newsite.html",
      terms: "Terms and conditions",
      termsLink: "https://www.canada.ca/en/transparency/terms.html",
      privacy: "Privacy",
      privacyLink: "https://www.canada.ca/en/transparency/privacy.html",
      imageAltText: "Symbol of the Government of Canada",

      aboutSite: "About this site",
      contactLink: "https://www.canada.ca/en/contact.html",
      contact: "Contact us",
      depLink: "https://www.canada.ca/en/government/dept.html",
      dep: "Departments and agencies",
      publicServiceLink: "https://www.canada.ca/en/government/publicservice.html",
      publicService: "Public service and military",
      newsLink: "http://news.gc.ca/web/index-en.do",
      news: "News",
      lawsLink: "https://www.canada.ca/en/government/system/laws.html",
      laws: "Treaties, laws and regulations",
      reportLink: "https://www.canada.ca/en/transparency/reporting.html",
      report: "Government-wide reporting",
      primeMinisterLink: "http://pm.gc.ca/en",
      primeMinister: "Prime Minister",
      systemLink: "https://www.canada.ca/en/government/system.html",
      system: "About government",
      openGovLink: "http://open.canada.ca/en",
      openGov: "Open government",

      pageUp: "Top of page",

      reportIssue: {
        buttonLink: "https://www.canada.ca/en/report-problem.html",
        buttonText: "Report a problem or mistake on this page"
      },

      shareButton: "Share this page",

      sharePanel: {
        header: "Share this page",
        bitly: "",
        blogger: "",
        digg: "",
        diigo: "",
        email: "",
        emailLabel: "Email",
        facebook: "",
        gmail: "",
        linkedin: "",
        myspace: "",
        pinterest: "",
        reddit: "",
        tumblr: "",
        twitter: "",
        yahoomail: "",
        bottomText: "No endorsement of any products or services is expressed or implied.",
        closePanel: "Close",
        closePanelHover1: "Close overlay",
        closePanelHover2: "Close overlay (escape key)"
      }
    },

    splashPage: {
      title: "Canada.ca",
      langSelectEnglish: "English",
      langSelectFrench: "Français",
      termsEnglish: "Terms\u00A0& Conditions", // The unicode character here is a non-breaking space (&nbsp). This is needed to keep the first 2 words together
      termsFrench: "Avis",
      termsLinkEnglish: "https://www.canada.ca/en/transparency/terms.html",
      termsLinkFrench: "https://www.canada.ca/fr/transparence/avis.html"
    },

    homePage: {
      title: "CAT Stack - Home",
      content1: "Edit src/pages/HomePage.jsx and save to reload.",
      content2: "Learn React",
      postAPIButton: "POST to api/sample-endpoint/",
      postAPIButtonLabel: "POST to API Endpoint",
      errorButton: "Show Error"
    },

    examplePage: {
      title: "CAT Stack - Example",
      content1:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at ipsum vitae diam aliquet ornare. Vestibulum non diam lacinia, scelerisque augue fringilla, convallis erat. Proin congue magna sit amet nisi pulvinar, sed hendrerit leo sagittis. Cras viverra sem non tincidunt maximus. Phasellus molestie purus vel mollis gravida. Morbi lacinia quam vitae enim commodo, sit amet rutrum neque euismod. Donec nec velit tortor. Curabitur in dolor laoreet arcu vestibulum semper ac nec eros. Cras fermentum lacus in molestie gravida."
    },

    loginPage: {
      title: "CAT Stack - Login",
      usernameHeader: "Username",
      passwordHeader: "Password",
      passwordAriaLabel: "Password field selected.",
      loginButton: "Login",
      loginError: "Login error, please try again."
    },

    adminPage: {
      title: "CAT Stack - Admin",
      content1: "Congratulations! You are now logged in."
    },

    error404page: {
      title: "Error 404--Not Found",
      subtitle: "From RFC 2068 Hypertext Transfer Protocol -- HTTP/1.1:",
      subtitle2: "10.4.5 404 Not Found",
      content1:
        "The server has not found anything matching the Request-URI. No indication is given of whether the condition is temporary or permanent.",
      content2:
        "If the server does not wish to make this information available to the client, the statuscode 403 (Forbidden) can be used instead. The 410 (Gone) status code SHOULD be used if the server knows, through some internally configurable mechanism, that an old resource is permanently unavailable and has no forwarding address."
    },

    unexpectedErrorPage: {
      title: "Unexpected Error",
      subtitle: "We apologize for the inconvenience",
      content1: "An unexpected error has occurred, and our Administrators have been alerted.",
      content2: "Please navigate to a valid web page in the meantime."
    }
  },

  // ======================
  // FRENCH
  // ======================
  fr: {
    navigation: {
      nextButton: "Suivant ››",
      nextButtonLabel: "Suivant",
      previousButton: "‹‹ Précédent",
      previousButtonLabel: "Précédent",
      startButton: "Débutez",
      finishButton: "Terminer",
      newActivityButton: "Nouvelle activité",
      questionnaireButton: "Questionnaire",

      a11y: {
        skipToMain: "Passer au contenu principal",
        skipToFooter: "Passer à « Au sujet du gouvernement »"
      }
    },

    language: {
      otherLanguage: "English",
      otherLanguageCode: "en"
    },

    alerts: {
      errorAlt: "Erreur",
      message: "Nom d'erreur ici"
    },

    timeoutPopup: {
      title: "Session Expirée",
      description:
        "Votre session a expiré en raison de l'inactivité. Vous allez maintenant être redirigé à la page d'accueil.",
      closeButton: "Fermer"
    },

    pageDetails: {
      dateModifiedText: "Date de modification : "
    },

    header: {
      otherLanguage: "English",
      otherLanguageCode: "en",
      canadaHomeLink: "https://www.canada.ca/fr.html",
      canadaHomeText: "Gouvernement du Canada",

      langSelectSection: "Sélection de la langue",

      search: "Recherche",
      searchLink: "https://www.canada.ca/fr/sr/srb.html",
      searchDefault: "Rechercher dans Canada.ca",

      menu: [
        ["Emplois et milieu de travail", "https://www.canada.ca/fr/services/emplois.html"],
        [
          "Immigration et citoyenneté",
          "https://www.canada.ca/fr/services/immigration-citoyennete.html"
        ],
        ["Voyage et tourisme", "https://voyage.gc.ca/"],
        ["Entreprises et industrie", "https://www.canada.ca/fr/services/entreprises.html"],
        ["Prestations", "https://www.canada.ca/fr/services/prestations.html"],
        ["Santé", "https://www.canada.ca/fr/services/sante.html"],
        ["Impôts", "https://www.canada.ca/fr/services/impots.html"],
        [
          "Environnement et ressources naturelles",
          "https://www.canada.ca/fr/services/environnement.html"
        ],
        ["Sécurité nationale et défense", "https://www.canada.ca/fr/services/defense.html"],
        ["Culture, histoire et sport", "https://www.canada.ca/fr/services/culture.html"],
        [
          "Services de police, justice et urgences",
          "https://www.canada.ca/fr/services/police.html"
        ],
        ["Transport et infrastructure", "https://www.canada.ca/fr/services/transport.html"],
        ["Canada et le monde", "https://international.gc.ca/world-monde/index.aspx?lang=fra"],
        ["Argent et finances", "https://www.canada.ca/fr/services/finance.html"],
        ["Science et innovation", "https://www.canada.ca/fr/services/science.html"]
      ],

      loginState: {
        loginText: "Connexion",
        logoutText: "Déconnexion",
        usernamePrefix: "Connecté en tant que :"
      },

      breadcrumb: {
        header: "Fil d'Ariane",
        homeText: "Acceuil",
        homeLink: "http://www.canada.ca/fr/index.html",

        pscText: "Accueil CFP",
        pscLink: "https://www.canada.ca/fr/commission-fonction-publique.html",

        breadcrumbText: "Fil d'Ariane",
        breadcrumbLink:
          "https://conception.canada.ca/configurations-conception-communes/fil-ariane.html"
      }
    },

    footer: {
      h2: "Au sujet du gouvernement",
      feedback: "Rétroaction",
      feedbackLink: "https://www1.canada.ca/fr/contact/retroaction.html",
      socialMedia: "Médias sociaux",
      socialMediaLink: "https://www.canada.ca/fr/sociaux.html",
      mobile: "Applications mobiles",
      mobileLink: "https://www.canada.ca/fr/mobile.html",
      about: "À propos de Canada.ca",
      aboutLink: "https://www1.canada.ca/fr/nouveausite.html",
      terms: "Avis",
      termsLink: "https://www.canada.ca/fr/transparence/avis.html",
      privacy: "Confidentialité",
      privacyLink: "https://www.canada.ca/fr/transparence/confidentialite.html",
      imageAltText: "Symbole du gouvernement du Canada",

      aboutSite: "À propos de ce site",
      contactLink: "https://www.canada.ca/fr/contact.html",
      contact: "Contactez-nous",
      depLink: "https://www.canada.ca/fr/gouvernement/min.html",
      dep: "Ministères et organismes",
      publicServiceLink: "https://www.canada.ca/fr/gouvernement/fonctionpublique.html",
      publicService: "Fonction publique et force militaire",
      newsLink: "https://www.canada.ca/fr/nouvelles.html",
      news: "Nouvelles",
      lawsLink: "https://www.canada.ca/fr/gouvernement/systeme/lois.html",
      laws: "Traités, lois et règlements",
      reportLink: "https://www.canada.ca/fr/transparence/rapports.html",
      report: "Rapports à l'échelle du gouvernement",
      primeMinisterLink: "http://pm.gc.ca/fr",
      primeMinister: "Premier ministre",
      systemLink: "https://www.canada.ca/fr/gouvernement/systeme.html",
      system: "À propos du gouvernement",
      openGovLink: "http://ouvert.canada.ca/",
      openGov: "Gouvernement ouvert",

      pageUp: "Haut de la page",

      reportIssue: {
        buttonLink: "https://www.canada.ca/fr/signaler-probleme.html",
        buttonText: "Signaler un problème ou une erreur sur cette page"
      },

      shareButton: "Partagez cette page",

      sharePanel: {
        header: "Partagez cette page",
        bitly: "",
        blogger: "",
        digg: "",
        diigo: "",
        email: "",
        emailLabel: "Courriel",
        facebook: "",
        gmail: "",
        linkedin: "",
        myspace: "",
        pinterest: "",
        reddit: "",
        tumblr: "",
        twitter: "",
        yahoomail: "",
        bottomText:
          "Aucun appui n’est accordé, soit de façon expresse ou tacite, à aucun produit ou service.",
        closePanel: "Fermer",
        closePanelHover1: "Fermer la fenêtre superposée",
        closePanelHover2: "Fermer la fenêtre superposée (touche d'échappement)"
      }
    },

    splashPage: {
      title: "Canada.ca",
      langSelectEnglish: "English",
      langSelectFrench: "Français",
      termsEnglish: "Terms\u00A0& Conditions", // The unicode character here is a non-breaking space (&nbsp). This is needed to keep the first 2 words together
      termsFrench: "Avis",
      termsLinkEnglish: "https://www.canada.ca/en/transparency/terms.html",
      termsLinkFrench: "https://www.canada.ca/fr/transparence/avis.html"
    },

    homePage: {
      title: "CAT Stack - Accueil",
      content1: "Modifier src/pages/HomePage.jsx et enregistrer pour recharger.",
      content2: "Apprenez React",
      postAPIButton: "POST à api/sample-endpoint/",
      postAPIButtonLabel: "POST à point de terminaison d'API",
      errorButton: "Montre erreur"
    },

    examplePage: {
      title: "CAT Stack - Exemple",
      content1:
        "Praesent semper nibh eget libero dapibus, id blandit ligula dictum. Suspendisse condimentum, massa ut blandit malesuada, elit tellus sagittis diam, accumsan porta tellus quam vitae lorem. Suspendisse magna massa, lacinia sit amet sagittis in, elementum vel felis. Aenean varius mollis libero. Praesent pharetra nisi id enim varius pharetra. Etiam dignissim, magna in sollicitudin viverra, libero dolor blandit metus, molestie posuere urna elit posuere lectus."
    },

    loginPage: {
      title: "CAT Stack - Login",
      usernameHeader: "Nom d'utilisateur",
      passwordHeader: "Mot de passe",
      passwordAriaLabel: "Champ de mot de passe sélectionné.",
      loginButton: "Connexion",
      loginError: "Erreur de connexion, veuillez réessayer."
    },

    adminPage: {
      title: "CAT Stack - Admin",
      content1: "Félicitations! Vous êtes maintenant connecté."
    },

    error404page: {
      title: "Erreur 404 - Page non trouvée",
      subtitle: "De RFC 2068 Hypertext Transfer Protocol -- HTTP/1.1:",
      subtitle2: "10.4.5 404 introuvable",
      content1:
        "Le serveur n'a rien trouvé correspondant à l'URI demandée. Aucune indication n'est donnée quant à savoir si la condition est temporaire ou permanente.",
      content2:
        "Si le serveur ne souhaite pas rendre cette information disponible au client, le code d'état 403 (Interdit) peut être utilisé à la place. Le code d'état 410 (Parti) DEVRAIT être utilisé si le serveur sait, via un mécanisme interne configurable, qu'une ancienne ressource est indisponible en permanence et qu'il n'y a pas d'adresse de redirection."
    },

    unexpectedErrorPage: {
      title: "Erreur Inattendue",
      subtitle: "Nous nous excusons pour tout inconvénient",
      content1: "Une erreur inattendue est survenue et nos Administrateurs ont été alertés.",
      content2: "Veuillez s'il vous plaît vous rendre à une page web valide entre-temps."
    }
  }
});

export default LOCALIZE;
