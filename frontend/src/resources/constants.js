/**
 * This file contains the important & centralized constants used throughout the application
 */

/**
 * Navigation paths/URLs
 */
// eslint-disable-next-line import/prefer-default-export
export const PATH = {
  splash: "/",
  home: "/home",
  example: "/ex",
  login: "/login",
  admin: "/admin"
};
