import React from "react";
import { shallow } from "enzyme";

// Components imports
import { App } from "../App";

describe("App component", () => {
  it("should render", () => {
    const wrapper = shallow(<App />);
    // Check if page actually rendered
    expect(wrapper.exists()).toBe(true);
  });
});
