import { setCurrentUserInfo, setLoginState } from "../../redux/loginActions";
import { resetRedux } from "../../redux/otherActions";
import loginReducer, { initialState } from "../../redux/loginReducer";

describe("loginReducer", () => {
  it("should return the initial state by default", () => {
    expect(loginReducer(undefined, {})).toEqual(initialState);
  });

  it("should return the initial state if the redux state is reset", () => {
    expect(loginReducer(undefined, resetRedux())).toEqual(initialState);
  });

  it("should set the login state properly based on the setLoginState action", () => {
    expect(loginReducer(undefined, setLoginState(true))).toEqual({
      ...initialState,
      isLoggedIn: true
    });
    expect(loginReducer(undefined, setLoginState(false))).toEqual({
      ...initialState,
      isLoggedIn: false
    });
  });

  it("should set the userInfo field properly based on the setCurrentUserInfo action", () => {
    const userInfo = { username: "Test Username" };

    expect(loginReducer(undefined, setCurrentUserInfo(userInfo))).toEqual({
      ...initialState,
      userInfo
    });
  });
});
