import { setLanguage, initialState } from "../../redux/localizationActions";
import localizationReducer from "../../redux/localizationReducer";

describe("localizationReducer", () => {
  it("Should have the language set to English by default;", () => {
    expect(initialState).toEqual({ language: "en" });
  });

  it("Should update language;", () => {
    const setFrenchLocality = setLanguage("fr");
    expect(localizationReducer(initialState, setFrenchLocality)).toEqual({ language: "fr" });

    const setEnglishLocality = setLanguage("en");
    expect(localizationReducer(initialState, setEnglishLocality)).toEqual({ language: "en" });
  });
});
