import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Route, Switch } from "react-router-dom";

// Constants
import { PATH } from "./resources/constants";

// Components
import PrivateRoute from "./components/commons/PrivateRoute";
import SplashPage from "./pages/SplashPage";
import HomePage from "./pages/HomePage";
import ExamplePage from "./pages/ExamplePage";
import LoginPage from "./pages/LoginPage";
import AdminPage from "./pages/AdminPage";
import Error404Page from "./pages/Error404Page";

export const RoutesList = props => {
  const { isLoggedIn } = props;

  return (
    <Switch>
      <Route path={PATH.splash} exact component={SplashPage} />
      <Route path={PATH.home} exact component={HomePage} />
      <Route path={PATH.example} exact component={ExamplePage} />

      <PrivateRoute
        exact
        auth={!isLoggedIn}
        path={PATH.login}
        component={LoginPage}
        redirectTo={PATH.home}
      />

      <PrivateRoute
        exact
        auth={isLoggedIn}
        path={PATH.admin}
        component={AdminPage}
        redirectTo={PATH.login}
      />

      {/* Handles all other routes; has to be last, since Switch maps for the first match */}
      <Route component={Error404Page} />
    </Switch>
  );
};

RoutesList.propTypes = {
  isLoggedIn: PropTypes.bool
};

RoutesList.defaultProps = {
  isLoggedIn: false
};

const mapStateToProps = state => {
  return {
    currentLanguage: state.localizationReducer.language,
    isLoggedIn: state.loginReducer.isLoggedIn
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RoutesList);
