// ========================================================================================
// This file contains helper functions for frontend logging
// ========================================================================================

import log from "loglevel";
import { getFormattedJWTnoAuth } from "./jwtUtil";

/**
 * Format of the logs when sent to the API
 * @param {*} log
 */
const logFormat = log => {
  const { level, message, stacktrace, timestamp } = log;
  const logObjJSON = {
    timestamp,
    severity: level.label.toUpperCase(),
    message,
    stacktrace
  };

  return logObjJSON;
};

/**
 * Options for loglevel-plugin-remote
 * Details: https://github.com/kutuluk/loglevel-plugin-remote
 */
export const remoteOptions = {
  url: "/api/add-log/",
  method: "POST",
  headers: {},
  token:
    sessionStorage.getItem("auth_token_noAuth") !== null
      ? JSON.parse(sessionStorage.getItem("auth_token_noAuth")).access
      : "tempToken",
  interval: 2000,
  level: "error",
  backoff: {
    multiplier: 2,
    jitter: 0.1,
    limit: 512000
  },
  capacity: 5,
  stacktrace: {
    levels: ["trace", "warn", "error"],
    depth: 3,
    excess: 0
  },
  timestamp: () => new Date().toISOString(),
  format: logFormat
};

/**
 * Function that actually logs error data & attempts to send it remotely to the API
 * Includes behaviour to handle failure cases
 * @param {} error
 * @param {*} info
 */
export const logError = async (error, info) => {
  await getFormattedJWTnoAuth(); // This ensures the correct JWT is obtained before logLevel makes any API requests
  log.error(error, info);
};

export default logError;
