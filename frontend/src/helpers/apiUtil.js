// ========================================================================================
// This file contains some helper functions to communicate with the backend API
// ========================================================================================

import { logError } from "./logUtil";
import { getFormattedJWTnoAuth } from "./jwtUtil";

// Sending the survey stats to the backend
export const sendDataToSampleEndpoint = async data => {
  const authToken = await getFormattedJWTnoAuth();
  const response = await fetch("/api/sample-endpoint/", {
    method: "POST",
    headers: {
      Authorization: authToken,
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });

  // Log a custom error if the API is unreachable
  if (!response.ok) {
    const error = new Error("Cannot send survey stats data to API");
    const info = "Failure did not occur in a React component";
    logError(error, info);
  }
};

export default sendDataToSampleEndpoint;
