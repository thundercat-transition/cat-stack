import { SET_LOGIN_STATE, SET_CURRENT_USER_INFO } from "./actionTypes";

export const setLoginState = isLoggedIn => {
  return {
    type: SET_LOGIN_STATE,
    payload: isLoggedIn
  };
};

export const setCurrentUserInfo = userInfo => {
  return {
    type: SET_CURRENT_USER_INFO,
    payload: userInfo
  };
};

export default setLoginState;
