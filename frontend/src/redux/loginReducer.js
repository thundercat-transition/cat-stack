import { SET_CURRENT_USER_INFO, SET_LOGIN_STATE, RESET_REDUX } from "./actionTypes";

export const initialState = {
  isLoggedIn: false,
  userInfo: {
    username: null
  }
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESET_REDUX:
      return { ...initialState };

    case SET_LOGIN_STATE:
      return {
        ...state,
        isLoggedIn: action.payload
      };

    case SET_CURRENT_USER_INFO:
      return {
        ...state,
        userInfo: action.payload
      };

    default:
      return state;
  }
};

export default loginReducer;
