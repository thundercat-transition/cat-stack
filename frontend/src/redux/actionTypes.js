/**
 * This file contains all of the action type names (constants)
 * Action names are separated by their first alphabetical letter here
 */

export const RESET_REDUX = "RESET_REDUX";

export const SET_LANGUAGE = "SET_LANGUAGE";

export const SET_LOGIN_STATE = "SET_LOGIN_STATE";

export const SET_CURRENT_USER_INFO = "SET_CURRENT_USER_INFO";
