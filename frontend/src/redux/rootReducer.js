/**
 * This file combines all the reducers into 1 reducer
 */
import { combineReducers } from "redux";

// Individual reducers
import localizationReducer from "./localizationReducer";
import loginReducer from "./loginReducer";

export default combineReducers({
  localizationReducer,
  loginReducer
});
