import { SET_LANGUAGE } from "./actionTypes";
import LOCALIZE from "../resources/textResources";

export const LANGUAGES = {
  english: "en",
  french: "fr"
};

// Action creator
const setLanguage = language => {
  localStorage.setItem("appLanguage", language);
  LOCALIZE.setLanguage(language);
  return { type: SET_LANGUAGE, language };
};

const initializeLanguage = () => {
  // Default language is set to "en" when web page is loaded for the first time
  const language = localStorage.getItem("appLanguage") || "en";
  LOCALIZE.setLanguage(language);
  return language;
};

const initialState = {
  language: initializeLanguage()
};

export { setLanguage, initialState };
