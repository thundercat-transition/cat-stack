from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

# ========
# Views
# ========

from user_management.views import permissions, user_personal_info
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)
from backend.views import sample_view, logs_view, jwt_custom_view

schema_view = get_swagger_view(title="CAT Stack APIs")

urlpatterns = [
    url(r"^$", schema_view),
    url(r"^api/admin/", admin.site.urls),
    url(r"^api/auth/", include("djoser.urls")),
    url(r"^api/auth/", include("djoser.urls.authtoken")),
    url(r"^api/auth/", include("rest_framework.urls", namespace="rest_framework")),
    url(r"^api/auth/jwt/create_token_no_auth/",
        jwt_custom_view.CreateJWTtoken.as_view(), name='token_obtain_pair_noAuth'),
    url(r"^api/auth/jwt/create_token/",
        TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r"^api/auth/jwt/refresh_token/",
        TokenRefreshView.as_view(), name='token_refresh'),
    url(r"^api/auth/jwt/verify_token/",
        TokenVerifyView.as_view(), name='token_verify'),
    url(
        r"^api/update-user-personal-info",
        user_personal_info.UpdateUserPersonalInfo.as_view(),
    ),
    url(r"^api/sample-endpoint/", sample_view.AddSample.as_view()),
    url(r"^api/add-log/", logs_view.AddLogs.as_view()),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [url(r"^__debug__/", include(debug_toolbar.urls))]
